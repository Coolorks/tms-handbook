module.exports = ctx => ({
  head: [
    ['link', { rel: 'icon', href: '/PBST-Logo.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#787878' }],
    [`script
  type="text/javascript"
  src="https://crowdin.com/js/crowdjet/crowdjet.js"`, {}, ``],
    [`div id="crowdjet-container"
  data-organization-domain="pinewood-builders"
  data-project-id="8"
  style="bottom: 90px; right: 20px;"`, {}, ``]
    ,[`div
  id="crowdjet-expand-container"
  style="bottom: 10px; right: 20px;"`, {}, ``]

  ],

  dest: "public/",
  title: "TMS Handbook",
  description: "The unofficial TMS Handbook",

  locales: {
    /* This is where you place your general locale config */
    "/": {
      lang: "en-US",
    }/*,
  "/fr/": {
      lang: "fr-US",
      title: "Manuel du TMS",
      description: "Le manuel non-officiel du TMS",
    },*/
  },

  themeConfig: {
    repo: "https://gitlab.com/pinewood-builders/TMS-Handbook",
    editLinks: true,
    docsDir: "docs/",
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: "green",
      disableThemeIgnore: true,
    },

    algolia: ctx.isProd ? ({
      apiKey: '8d22e1eefdbc0248fed9c76b1d115c9f',
      indexName: 'pinewood-builders_tms'
  }) : null,

    logo: "/PBST-Logo.png",
    smoothScroll: true,

    locales: {
      "/": {
        // text for the language dropdown
        selectText: "Languages",
        // label for this locale in the language dropdown
        label: "English",
        // Aria Label for locale in the dropdown
        ariaLabel: "Languages",
        // text for the edit-on-github link
        editLinkText: "Edit this page on GitHub",
        nav: [
          {
            text: "Home",
            link: "/",
          },
          {
            text: "TMS-Handbook",
            link: "/tms/",
          },
          {
            text: "Pinewood",
            items: [
              {
                text: "PBST-Handbook",
                link: "https://pbst.pinewood-builders.com",
              },
              {
                text: "PET-Handbook",
                link: "https://pet.pinewood-builders.com",
              },
            ],
          },
        ],
      },
      /*"/fr/": {
        // text for the language dropdown
        selectText: "Langues",
        // label for this locale in the language dropdown
        label: "French",
        // Aria Label for locale in the dropdown
        ariaLabel: "Langues",
        // text for the edit-on-github link
        editLinkText: "Edit this page on GitHub",
        nav: [
          {
            text: "Accueil",
            link: "/fr/",
          },
          {
            text: "Manuel du TMS",
            link: "/fr/tms/",
          },
          {
            text: "Pinewood",
            items: [
              {
                text: "Manuel de la PBST",
                link: "https://pbst.pinewood-builders.com",
              },
              {
                text: "Manuel de la PET",
                link: "https://pet.pinewood-builders.com",
              },
            ],
          },
        ],
      },*/
    },
    sidebarDepth: 2,
    plugins: [
      [
        "@vuepress/pwa",
        {
          serviceWorker: true,
          updatePopup: true,
        },
      ],
    ], 
  }
}
)