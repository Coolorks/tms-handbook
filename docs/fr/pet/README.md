---
sidebar: auto
sidebarDepth: 5
---

# Manuel du TMS

Le Syndicat Mayhem est un groupe de Roblox opérant dans les complexes Pinewood Builders. Ce document établit les règles et les directives pour le TMS ainsi que les exigences des promotions.


## Système de promotions
A la fin des entrainements, les membres gagnent entre 1 et 7 points à ajouter dans la base de données en fonction de leur performance. A la fin des raids pour les points, les membres reçoivent un certain nombre de points en fonction du type de raid et de leur performance

Lorsqu'un membre a obtenu le nombre requis de points pour le prochain rang, il peut participer à une évaluation pour ce rang. Bien que l'évaluation de chaque rang a des exigences, la performance, l'attitude et le comportement passés de ce membre seront pris en compte.

Si un membre passe son évaluation, il sera promu au prochain rang. Sinon, les instructeurs décideront d'un retrait de point et/ou d'un temps d'attente pour que ce membre prenne le temps de s'entrainer.

## Règles générales
1. Ne sortez pas de votre chemin pour tuer les personnes neutres.
2. Ne changez pas entre la PBST et le TMS (ou vice-versa) pendant les raids.
3. Vous pouvez porter une combinaison HAZMAT de la PET, mais ne l'aidez pas à arrêter les événements comme une inondation radioactive, sauf si cet événement empêche le début d'une fonte ou d'un gel du noyau.

4. Si votre rang au dessus de votre tête est réglé sur TMS, vous ne devez pas aider à sauver le noyau.
5. Si votre rang au dessus de votre tête est réglé sur TMS, vous ne devez pas avoir d'armes ni d'uniformes de la PBST ou de la PET.
6. N'attaquez pas la PBST s'ils sont en chemin vers la salle de sécurité pour qu'ils puissent prendre leurs équipements. S'ils vous attaquent, vous pouvez ignorez cette règle pour ce combat.

7. N'importe qui avec un rang ET un uniforme PBST est à tuer immédiatement si vous êtes en service pour le TMS
8. Ne prenez pas d'outils de la PBST ou de la PET si votre rang au dessus de votre tête est réglé sur TMS.
9. Si votre rang au dessus de votre tête est réglé sur TMS, vous devez porter un uniforme du TMS, les uniformes customisés ne sont autorisés que si vous êtes un instructeur ou supérieur. Si vous ne voulez pas être en service pour le TMS, merci de ne pas avoir votre rang réglé sur TMS.

## Les promotions

### Soldat
**Points requis** : 40 points  
**Privilèges** :
 - Accès aux armes  
   **Evaluation** :
1. Finir les hautes tours du PBSTAC en moins de 90 secondes
2. Finir soit l'activité "SF bot" de niveau 1, soit l'activité "gun bot" de niveau 1
3. Répondre correctement à 5 questions sur 8 pendant un quiz sur les protocoles et les mécaniques du noyau
4. Accord de l'instructeur

### Agent
**Points requis** : 120 points    
**Privilèges** :
- Donner des ordres de KoS
- Restreindre des salles
- Organiser des raids pour 1 à 3 points

**Evaluation** :
1. Répondre correctement à 5 questions sur 6 à propos des situations spécifiques du noyau
2. Organiser un raid sous la supervision d'un instructeur
3. Accord de l'instructeur
### Capitaine
**Points requis** : 200 points + organiser 10 raids  
**Privilèges** :
- Ne pas tenir compte des restrictions et des ordres de KoS
- Organiser des raids pour 1 à 5 points

**Evaluation** :
1. Finir le barrage de bombes de niveau 2
2. Combattre 3 fois l'instructeur évaluateur. Vous pouvez choisir si vous voulez des épées ou des pistolets pour les deux premières fois. La troisième sera ce que vous n'aurez pas choisi
3. Accord de l'instructeur. Si vous avez réussi le barrage de bombes mais avez échouer à cette partie de l'évaluation, vous ne serez pas capable d'être réévalué si vous n'y êtes pas invité

### Instructeur
Doit être choisi par les instructeurs actuels et supérieurs.

Signé, l'administration du TMS

