---
sidebar: auto
sidebarDepth: 5
---
# TMS Handbook

## General Reminders
---
* Be respectful to other members, especially to those who outrank you, we’re all playing to have fun.
* Do not try to evade punishments, evading will only make it worse.
* We are not at war with other groups, hostility towards them is unwelcomed.
* **Spawnkilling and Mass Random-killing** is forbidden for anyone, at any time, and will be severely punished if caught.
 – **Spawn extends from the spawn lobby, through the entirety of the spawn shop, up the stairs, and down the elevator shaft. Spawn ends past the yellow line at the bottom of the elevator.** <br>
 – The PBRF elevator area attached to the spawn lobby and the Easy Escape room is also considered a spawn zone! <br>
 – Camping the bottom of the elevator shaft is frowned upon, try to attack elsewhere!
* Please follow the Roblox Community Rules at all times, this is still just a game!
* Attacking other on-duty TMS members is frowned upon and can lead to a punishment.
---

## Rules while raiding
These rules are for anyone who has their rank tag set to The Mayhem Syndicate (Red **Syndicate** title)

### Uniform
*While your rank tag is set to TMS, you are considered **on-duty** for TMS and are required to wear the official TMS uniform!* 
You can find the **TMS Hideout** at the Pinewood Computer Core near the Cargo trains platform.

The TMS hideout is located near the Cargo train spawn at Pinewood Computer Core

You are **not** allowed to wear a PBST uniform while on-duty as TMS, this is severely enforced if caught.

::: tip PET/Core Suits
You are allowed to wear PET suits or the core suit while on-duty as TMS. A uniform is not required while wearing these suits.
:::

Instructor+ are allowed to wear modified/custom uniforms.

If you have Donor Commands, you are able to give yourself a uniform through the !shirt and !pants command
Uniform Texture IDs
Shirts: 4428565863
Pants: 4428568303

Links for the Uniform
https://www.roblox.com/catalog/4428568331/TMS-Uniform-Pants 12
https://www.roblox.com/catalog/4428565917/TMS-Uniform-Shirt 3

Better yet, you can combine those two into an alias, run this command in chat
!addalias !tmsuniform !shirt 4428565863 & !pants 4428568303
Then, every time you run !tmsuniform in chat, you will put on the TMS uniform!

**You must have Donor Commands to do the above**

### Weapons
Syndicate members are given Weapons at the Pinewood Computer Core to assist in destroying the core or causing chaos, these can be found at the Hideout near Cargo trains. **Weapons become more powerful and diverse as you rank up.** More information regarding what tools can be achieved can be seen in the **Ranks** section.

Weapons given are not to be used while off-duty or on-duty for another group.

**You are not allowed to use PBST tools or PET tools (Fire Hose, Medical Box, or PET shield) while on-duty as TMS**

The Loadout area, or the TMS Hideout, is shown below in the photo. 

![img](https://doy2mn9upadnk.cloudfront.net/uploads/default/original/4X/5/6/5/565a06af900dc2a6d47d19f38640bfda4d2cffcc.jpeg)

### Encountering PBST
While raiding, you will most likely encounter the Security of the Computer Core. These members will actively attack you.

You are to **kill security on sight**, with a few exceptions.

* You cannot kill PBST if they are in spawn, they are protected by spawn restrictions.
* You cannot attack a security officer going to get their loadout, you may if they attack you first.
* The PBST member must be both **on-duty as PBST** and **wearing an official PBST uniform** in order to be attacked, if the member is a **Tier 4 Special Defense** rank or above in PBST, you may kill them if they don’t have a uniform on. You can ignore this rule if the PBST member is a threat and/or is interfering with your meltdown/freezedown objective.

There is a rare occasion where a PBST officer goes rogue and will help destroy the core, these people appear every so often.

### Neutrals
Do not go out of your way to attack neutrals while raiding, if they are not a threat or are not interfering with your objective, please do not kill them.

:::warning Please remember
While on-duty as a Syndicate member, you are expected to help cause chaos by melting or freezing the core. There are times where there are multiple Syndicate members trying to do different objectives, communication is key to success! Work together to meet a common goal and success will follow. 
:::

### PET/Core suits
You are allowed to wear PET suits or the core suit while on-duty as TMS. A uniform is not required while wearing these suits.

TMS are allowed to use the Astronaut helmet during raids, this can be used in the core when the temperature is below 3000.
Astronaut Helmet ID: *1081366*

### Events
Every so often at Pinewood Computer Core, a disaster will occur. These disasters do vary and can change the gameplay dramatically. These disaster are as follows;

* Meltdown
* Freezedown
* Gas Leak
* Earthquake
* Plasma Leak
* Blackout
* Radiation Leak
* Aliens

You are **not** allowed to fix the Radiation leak unless it is preventing meltdown or freezedown from occurring.

### Dealing with Rogue Syndicates
Every so often, you may encounter a syndicate member trying to save the core, breaking a game rule, team-killing other members, or abusing weapons. These members are considered Rogue. Depending on how they are acting, you may have to kill them in self-defense if they are attacking you on invalid terms.

If you run into a ranked individual (Trooper+) abusing their TMS specific weapons, send a Group Weapon Abuse call to PIA and note the weapons they are using.

Individuals are able to declare an individual rogue if they have the authority to issue a KoS order (Operative+).

Rogue Syndicates are;

* Teamkilling on purpose
* Spawnkilling or Mass random killing on purpose
* Breaking the handbook on a large scale

Operatives and up are able to directly file for a deduction on someone, but you can report the rulebreakers to them as well.

--- 
## Ranks
Ranks can give an individual additional authority or weapons. Trooper and Operative have a point cap, meaning you are set back to the point amount if you exceed the cap right after you are promoted. i.e. Promoted to trooper at 70 points, set back to 60.

### Recruit
Recruit is the entry rank into TMS. There is no evaluation for this rank and no point requirement.

#### Weapons
* Baby Crowbar (Low Damage)

### Trooper
Trooper is the first rank that can be achieved through an evaluation.

**Points Required**: 40 points <br>
**Point Cap**: 60 points

#### Weapons
* Crowbar
* Pistol
* Smoke Grenade

#### Evaluation
* Complete Tall Towers at PBSTAC in under 90 seconds
* Successful completion of one combat arena.
    * Level 1 Sword Bots or Level 1 Gun Bots
* A minimum of 5 questions correct out of a given 8 questions
* Minor Instructor Consensus

### Operative
Operative is the first rank that is allowed to host official raids within the Syndicate.

This rank and further ranks require you to be in the TMS discord server.
The invite is present in the social links on the TMS group, you must be 13 and up to join.


**Points Required**: 120 points <br>
**Point Cap**: 140 points

#### Weapons

* Crowbar
* Pistol
* Smoke Grenade
* Rifle

#### Evaluation
* Answer 5 questions correctly out of a given 6.
    * This must be completed successfully before moving onto the next stage.
* Host a raid observed by an Instructor
* Instructor Consensus

#### Authority
* Issue official TMS KoS on members in a server
* Restrict rooms to TMS only
* Host Official Raids (Level 0 and Level 1)

### Captain
Captain is the third achievable rank within the Syndicate and is trusted with more authority.

**Points Required**: 200 points <br>
**Point Cap**: *N/A*
#### Weapons

* Crowbar
* Pistol
* Smoke Grenade
* Rifle
* Submachine Gun

#### Evaluation
* Complete Level 2 Bomb Barrage
    * This must be completed successfully before moving onto the next stage.
* Fight the evaluating instructor 3 times
    * Two of these rounds are either Gun or Sword rounds.
    * Rifle, Submachine Gun, and Pistol are provided for the Gun round(s)
    * Gamma Spec is provided for the Sword round(s)
    * The third round is the item you did not choose. (i.e. 2 gun & 1 sword, 2 sword & 1 gun) (Not 3 gun or 3 sword rounds)
* Major Instructor Consensus
    * If you pass BB and finish the combat but fail the consensus, you must be invited by an Instructor to take the evaluation again. 

#### Authority
* Issue official TMS KoS on members in a server
* Restrict rooms to TMS only
* Host Official Raids (Level 0, Level 1, and Level 2)
* **Override KoS orders**
* **Override Room Restrictions**

### Instructor
Instructor is the highest achievable rank in the Syndicate.
These members are the leaders of the group; they handle operations, evaluations, points, and oversee a majority of events.

**Points Required**: N/A

#### Weapons

* Crowbar
* Upgraded Pistol
* Upgraded Rifle
* Upgraded Submachine Gun
* Katana
* Smoke Bomb

#### Evaluation
You are handpicked by the current Instructors or above and voted in by all Instructors. You can be voted in at any rank.

#### Authority
* These members have full authority over lower-ranking members.
* Instructors are able to host Level 0, Level 1, Level 2, and Level 3 raids.
* Able to host official group training sessions.

--- 

## Raid System
Official Raids are hosted often in the Mayhem Syndicate.
**You must be an Operative+ to host an Official Syndicate Raid.**

Points are able to be earned during an official raid. The amount given varies depending on the raid level and how long you attended it. Behavior and participation during the raid are taken into account.

### Level 0-3 raids
**Level 0 raid**
Level 0 raids award no points. These can be hosted by Operative+

**Level 1 raid**
Level 1 raids award up to 3 points. These can be hosted by Operative+

**Level 2 raid**
Level 2 raids award up to 5 points. These can be hosted by Captain+

**Level 3 raid**
Level 3 raids award up to 7 points. These are organized by Instructor+ and Captains are able to host a server during these MEGA raids.
These raids are considered MEGA raids and are multi-server raids.

::: danger Be aware! 
Raid hosts do have to follow rules regarding hosting, all hosts do. The rules these members have to follow are shown below under the Raid Hosting Rules section
:::

### Hosting Rules
All times of when a day starts or ends are decided by UTC.

* Hosts may host up to **2** raids a day **EACH**.
* Each host can only have **ONE** point raid a day **EACH**.
* If the last raid was a **level 2** raid, you must wait at least 4 hours after that raid started before you can host another.
* If the last raid was a **level 1** raid, you must wait at least 3 hours from the start time of the last raid.
* If the last raid was a **level 0** raid, you must wait at least 2 hours after that raid started before you can host another.
* There must be a **ONE** hour period between the end of any official raid and the start of the next official raid.
* Hosts cannot start a raid close to or within the hour before the PBST Mega-Training.

All hosts are expected to follow these. Instructors have the authority to end a raid early or cancel it if it violates the raid hosting rules.
There is no set cooldown for trainings, but raids cannot occur during a training session.

--- 
### Raid Types
The Mayhem Syndicate hold a variety of different raids, these can be seen below in detail.

#### Meltdown
Meltdown raids are the most common raid found at TMS. The goal is simple, cause a meltdown. This is done by raising the temp beyond 4000 degrees.

#### Freezedown
Freezedown raids are one of the rarer raid found at TMS. The goal is simple, cause a freezedown. This is done by lowering the temp below -4000 degrees.

#### Chaos
One of the most fun raid types, the Chaos raid! This raid is where TMS shines in it’s more chaotic stages. During these raids, massive fires, nuke trains explode, and core temperatures soar into unstable numbers.

#### Post-raid
This is not an objective, but it is an objective once the primary objective is reached after a Meltdown or Freezedown begins, it is to do a No-Survivor or Yes-Survivor run, or none at all.

* **Yes-Survivor**

  Help survivors escape in rockets

* **No-Survivor**
  
  Do not let anyone escape in rockets
  Do not let anyone escape in rockets

Once a pointed raid ends, TMS members are to head to the Pinewood Builders Data Storage Facility, with the ``!pbdsf`` command in chat. 
This is to group up, submit a point log for everyone who attended, then dismiss everyone.

--- 
### E-Coolant Overview
Last but not least, after a Meltdown starts, E-Coolant must be taken into account to prevent PBST from saving the core

E-Coolant, or Emergency Coolant, is a gameplay aspect that lets users have a chance at saving the core once a Meltdown begins. It is located in Sector G, both doors require the code 5334118 to open.

E-Coolant does play a critical role and must be kept under control, or risk getting the core temp set back to 3000 degrees.

In order for E-Coolant to be successful, all 3 of the rods must have their levels between 69 and 81 percent to be green. Going above or below those ranges will make the rod unusable.

If all three coolant rods are in the green and the countdown reaches 0, there is a 50 percent chance the coolant will work and return the core temp to 3000 degrees.

![img](https://doy2mn9upadnk.cloudfront.net/uploads/default/original/4X/1/c/7/1c72246ac9ee343543c7209c30e401dee2f2fec0.jpeg)

--- 
## Trainings
Trainings are hosted periodically and are done at any facility, even other games. Trainings can give 1 to 4 points depending on performance. Bad behavior during trainings can lead to a removal from the training and no points awarded. Negative points can also be issued if the situation requires it.

---
## Raid Essentials 
If you can't access the raid communication VC's, another way to effectivly communicate via ROBLOX chat is by the use of 10- codes. These codes allow you to easily convey essential information in only a few numbers, here are some of the most common ones: <br>

10-4 - Roger/Affirmative <br> 
10-33 - Help/Emergency - When using this code, make sure you also specify the location where you require help<br> 
10-22 - Disregard<br> 
10-17 - Responding <br> 

During a raid, there are 3 key areas, these include : <br> 
Core <br>
Coolant<br>
AC/Fans<br>
Make sure you're in atleast one of these areas or circulating around these areas during a raid. 

During a raid, the most important place you can be is in the core, if you notice there are lots of raiders where you currently are, head to the core and help out there.
When in Reactor Power, you can stick the barrel of your gun out into the reactor power entrance while keeping yourself behind the wall, this allows you to shoot incoming people without them being able to hit you.

![img](https://iili.io/JLbPLu.png)

:::tip 
If you are tasered, you can easily escape the stun by using your jetpack. <br>
![img](https://iili.io/JZPxBR.png) <br>
It will only work if you use the jetpack shown above <br>
:::





---
::: danger Please note this!
This handbook is always a work in progress. 
:::

---



*Glory to the Syndicate!*

Signed,
*The Instructors and the Architect*

***Last updated**: 5/26/2020*
